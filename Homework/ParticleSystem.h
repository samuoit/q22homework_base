#pragma once

#include <time.h>
#include <GL/glew.h>
#include "MiniMath\Core.h"
#include "ShaderProgram.h"
#include "Mesh.h"
#include "Texture.h"

struct VBOReadyData
{
	vec4 *Quads = nullptr;			// Corners making up particles geometry
	vec2 *TextureUVs = nullptr;		// Texture coordinates of particles
	float *Alphas = nullptr;		// Alpha blending factor of particles
};

struct ParticlesData
{
	vec3 *Positions = nullptr;		// Centers of particles
	vec3 *Velocities = nullptr;		// Velocities of particles
	float *Size = nullptr;			// Sizes of particles (width and height assumed to be the same)
	float *Alpha = nullptr;			// Color alpha values for particles
	float *Ages = nullptr;			// Ages of particles
	float *Lifetimes = nullptr;		// Lifetimes of particles
};

class ParticleSystem
{
public:
	ParticleSystem();
	~ParticleSystem();

	bool Init														// Initialize particles
		(
		vec3 positionMinimum,										// Minimum position where particle can be generated
		vec3 positionMaximum,										// Maximum position where particle can be generated
		vec3 velocityMinimum,										// Minimum velocity of particle in X, Y, and Z directions
		vec3 velocityMaximum,										// Maximum velocity of particle in X, Y, and Z directions
		vec2 lifeTimeRangeMinMax,									// Minimum and maximum life time of a particle (x - min, y - max)
		vec2 alphaRangeMinMax,										// Minimum and maximum alpha range (x - min, y - max)
		vec2 sizeRangeMinMax,										// Minimum and maximum size range (x - min, y - max)
		unsigned int maxParticles,									// Maximum number of particles to spawn at a time
		unsigned int rate,											// Rate of spawning
		const char* fileParticlePath								// Path to particle texture
		);

	void Update(float elapsed);										// Update particles
	void Draw();													// Draw particles

	void SetMatrices												// Set matrices used by shaders
		(
		mat4 projectionSpace = mat4::Identity(),
		mat4 viewSpace = mat4::Identity(),
		mat4 worldSpace = mat4::Identity()
		);

private:

	// Handles to VAO and VBO buffers
	GLuint vao = 0;
	GLuint vboQuads = 0;
	GLuint vboTexUVs = 0;
	GLuint vboAlphas = 0;

	// Shader program and shaders
	ShaderProgram progParticleSystem;
	Mesh vertParticles, fragParticles;

	// Particles related
	VBOReadyData vboData;								// Data sent to vertex shader
	ParticlesData particlesList;						// Data used to track particle states
	Texture particleTexture;							// Particle texture
	const char* particleTextureName;					// Path to particle texture file
	float emitRate = 0.0f;								// Rate at which particles are generated
	unsigned int maxParticles = 0;						// Maximum particles allowed
	unsigned int particlesCreated = 0;					// Count of currently created particles

	unsigned int geomObjSize = 0;						// Size of the geometric object created out of triangles

	vec3 positionRangeMinimum, positionRangeMaximum;	// Position ranges minimum and maximum on all axis
	vec3 velocityRangeMinimum, velocityRangeMaximum;	// Velocity ranges minimum and maximum on all axis
	vec2 lifetimeRange;									// Life time range (x - minimum, y - maximum)
	vec2 alphaRange;									// Lerp for vertex color alpha (x - minimum, y - maximum)
	vec2 sizeRange;										// Lerp size (x - minimum, y - maximum)

	// Matrices used by shaders
	mat4 projectionMatrix;								// Projection plane
	mat4 viewMatrix;									// Viewing origin and orientation
	mat4 modelMatrix;									// Particle spawn origin in world
};