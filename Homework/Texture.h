#pragma once
#include <GL/glew.h>
#include <string>

class Texture
{
public:
	Texture() = default;
	~Texture();

	bool Load(const std::string &file);
	void Unload();

	void Bind();
	void Unbind();

	// - The handle to a texture object
	GLuint TexObj = 0;

private:

};