#version 420

// Uniforms
uniform mat4 uModel;
uniform mat4 uView;
uniform mat4 uProj;

// Data from C++
layout(location = 0) in vec4 posIn;
layout(location = 1) in vec2 texUVsIn;
layout(location = 2) in float alphaIn;

// Vertex outputs
out vec2 texcoord;
out float alpha;

void main()
{
	gl_Position = uProj * uView * uModel * posIn;			// Calculate vertex position
	texcoord = texUVsIn;									// Pass texture UVs
	alpha = alphaIn;										// Pass alpha
}