#version 420

// Inputs
in float size[];
in float alpha[];

layout (points) in;
layout (triangle_strip, max_vertices = 4) out;

// Outputs
out vec2 texcoord;
out float frag_alpha;

// Uniforms
uniform mat4 uProj;

void main()
{
	// Position passed by vertex shader
	vec3 vertPos = gl_in[0].gl_Position.xyz;

	// Texture coordinates for corners of quad to create
	vec2 textureCoords[4] = { vec2(0.0, 0.0), vec2(1.0, 0.0), vec2(0.0, 1.0), vec2(1.0, 1.0) };
	
	// Vertex modifiers based on position of the corner and size of particle
	vec2 vertModXY[4] = { vec2(-0.5, -0.5) * size[0], vec2(0.5, -0.5) * size[0], vec2(-0.5, 0.5) * size[0], vec2(0.5, 0.5) * size[0] };

	// Create quad (from position and specified size)
	for(int i = 0; i < 4; i++)
	{
		gl_Position = uProj * vec4(vertPos.x + vertModXY[i].x, vertPos.y + vertModXY[i].y, vertPos.z, 1.0); 
		texcoord = textureCoords[i];
		frag_alpha = alpha[0];
		EmitVertex();							// Output vertex
	}

	// End primitive - We just need 4 corners for quad
	EndPrimitive();
}