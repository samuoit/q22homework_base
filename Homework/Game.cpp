#include "Game.h"
#include "Utilities.h"

Game::Game() {}

Game::~Game()
{
	delete updateTimer;
	ParticleProgram.UnLoad();
}

void Game::initializeGame()
{
	updateTimer = new Timer();

	InitFullScreenQuad();

	glEnable(GL_DEPTH_TEST);
	
	// Particle system initialization
	if (!particleSystem.Init(
		vec3(-15.0f, -10.0, -15.0f),			// Minimum position of particles
		vec3(15.0f, 10.0f, -2.0f),				// Maximum position of particles
		vec3(-5.0f, -5.0f, -5.0f),				// Miminum velocity of particles
		vec3(5.0f, 5.0f, 5.0f),					// Maximum velocity of particles
		vec2(4.0f, 15.0f),						// Life time range
		vec2(0.1f, 0.8f),						// Alpha blending range
		vec2(0.5f, 2.0f),						// Size range
		400,									// Maximum number of particles spawned at a time
		10,										// Rate of particles spawned
		"./Assets/Textures/particle_snow1.png"	// Custom texture file path if particles used are PARTICLE_CUSTOM
		))
	{
		std::cout << "Particle system failed to initialize.\n";
		system("pause");
		exit(0);
	}

	CameraTransform.Translate(0.0f, 0.0f, 5.0f);
	CameraProjection.FrustumProjection(60.0f, (float)WINDOW_WIDTH / (float)WINDOW_HEIGHT, 1.0f, 10000.0f);

	ParticleSystemTransform.Translate(vec3(1.0f, 1.0f, -5.0f));
	particleSystem.SetMatrices(CameraProjection, CameraTransform.GetInverse(), ParticleSystemTransform);
}

void Game::update()
{
	// update our clock so we have the delta time since the last update
	updateTimer->tick();
	float deltaTime = updateTimer->getElapsedTimeSeconds();
	TotalGameTime += deltaTime;

	// update particle system
	particleSystem.Update(deltaTime);
}

void Game::draw()
{
	glClearColor(0.1f, 0.1f, 0.1f, 0.0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	// draw particles
	particleSystem.Draw();
	
	glutSwapBuffers();
}

void Game::keyboardDown(unsigned char key, int mouseX, int mouseY)
{
	switch(key)
	{
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	}
}

void Game::keyboardUp(unsigned char key, int mouseX, int mouseY)
{
	switch(key)
	{
	case 32: // the space bar
		break;
	case 27: // the escape key
	case 'q': // the 'q' key
		exit(1);
		break;
	}
}

void Game::mouseClicked(int button, int state, int x, int y)
{
	if(state == GLUT_DOWN) 
	{
		switch(button)
		{
		case GLUT_LEFT_BUTTON:

			break;
		case GLUT_RIGHT_BUTTON:
		
			break;
		case GLUT_MIDDLE_BUTTON:

			break;
		}
	}
	else
	{

	}
}

void Game::mouseMoved(int x, int y)
{
}
